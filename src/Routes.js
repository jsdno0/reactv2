
import React, { Fragment, Component }  from 'react';
import { BrowserRouter, Switch, Redirect, Route, } from 'react-router-dom';
import {SampleContainer} from './sample';

const Routes = () => (
  <BrowserRouter basename="/">
    <Switch>
    <Redirect exact from="/" to="/sample"/>
      <Route path={`/sample`} component={SampleContainer} />
    </Switch>
  </BrowserRouter>
);



export default Routes;
